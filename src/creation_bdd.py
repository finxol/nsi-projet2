import sqlite3
import csv

# Ouverture base de données
c = sqlite3.connect("bdd.db")

#############################
# Création table des villes #
#############################
# c.execute("drop table if exists Villes;")
# c.execute("create table Villes "
# "(id_ville text primary key, cp text, commune text, dept text, region text, coord text);")
# 
# # Population de la table villes
# fichier = open("correspondance-code-insee-code-postal.csv", "r", encoding='utf-8')
# donnees = []
# reader = csv.DictReader(fichier, delimiter=";")
# 
# for ligne in reader:
#     d = ligne['\ufeff"Code INSEE,Code Postal,Commune,Département,Région,geo_point_2d"'].split(',', 5)
#     query = f'INSERT INTO Villes VALUES ("{d[0]}", {d[1]}, "{d[2]}", "{d[3]}", "{d[4]}", {d[5]});'
#     print(query)
#     c.execute(query)
# 
# fichier.close


##############################
# Création table des hobbies #
##############################
c.execute("DROP TABLE IF EXISTS Hobbies;")
c.execute(
    "CREATE TABLE Hobbies (id_hobbies INTEGER PRIMARY KEY AUTOINCREMENT, freq_musique VARCHAR(28), freq_cinema "
    "VARCHAR(28), freq_litterature VARCHAR(28), freq_sport VARCHAR(28), FOREIGN KEY(id_hobbies) REFERENCES Profils("
    "id_util));")

#######################################
# Création table des traits physiques #
#######################################
c.execute("DROP TABLE IF EXISTS Physique;")
c.execute(
    "CREATE TABLE Physique (id_phys INTEGER PRIMARY KEY AUTOINCREMENT, coul_yeux VARCHAR(28), coul_cheveux VARCHAR("
    "28), taille INTEGER, poids INTEGER, imc INTEGER, musculature VARCHAR(28), FOREIGN KEY(id_phys) REFERENCES "
    "Profils(id_util));")

##############################
# Création table des profils #
##############################
c.execute("DROP TABLE IF EXISTS Profils;")
c.execute(
    "CREATE TABLE Profils (id_util INTEGER PRIMARY KEY AUTOINCREMENT, nom VARCHAR(28), prenom VARCHAR(28), "
    "age INTEGER, mail TEXT, sexe VARCHAR(28), orientation VARCHAR(28), ville INTEGER, FOREIGN KEY(ville) REFERENCES "
    "Villes(id_ville));")

###########################
# Ajout entrée 0 (tests) #
###########################
c.execute("INSERT INTO Hobbies VALUES (0, 'souvent', 'souvent', 'souvent', 'souvent');")
c.execute("INSERT INTO Physique VALUES (0, 'bleu', 'blond', 188, 71, 20, 'forte');")
c.execute("INSERT INTO Profils VALUES (0, 'Durand', 'Michel', 25, 'm.durand@gmail.com', 'h', 'f', 25418);")

###########################
# Ajout entrée 1 (tests) #
###########################
c.execute("INSERT INTO Hobbies VALUES (1, 'parfois', 'souvent', 'rarement', 'jamais');")
c.execute("INSERT INTO Physique VALUES (1, 'marron', 'brun', 179, 75, 23, 'moyenne');")
c.execute("INSERT INTO Profils VALUES (1, 'Tisserande', 'Léonie', 30, 'l.tisserande@gmail.com', 'f', 'h', 25056);")

###########################
# Ajout entrée 2 (tests) #
###########################
c.execute("INSERT INTO Hobbies VALUES (2, 'jamais', 'parfois', 'souvent', 'rarement');")
c.execute("INSERT INTO Physique VALUES (2, 'gris', 'roux', 158, 61, 24, 'forte');")
c.execute("INSERT INTO Profils VALUES (2, 'Emma', 'Dune', 19, 'e.dune@gmail.com', 'f', 'f', 56260);")

###########################
# Ajout entrée 3 (tests) #
###########################
c.execute("INSERT INTO Hobbies VALUES (3, 'parfois', 'parfois', 'souvent', 'souvent');")
c.execute("INSERT INTO Physique VALUES (3, 'noir', 'chatain', 169, 65, 23, 'moyenne');")
c.execute("INSERT INTO Profils VALUES (3, 'Damien', 'Ruzol', 19, 'd.ruzol@gmail.com', 'h', 'h', 35238);")

c.commit()
print("ok")
