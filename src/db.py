import sqlite3

TOUTES_COLONES = "id_util, nom, prenom, age, mail, sexe, orientation, id_ville, commune, freq_musique, freq_cinema, " \
                 "freq_litterature, freq_sport, coul_yeux, coul_cheveux, taille, poids, imc, musculature"


def insert_sql(table, keys, values):
    """
        Exécute une requête SQL

        Paramètres 
        -----------
        keys : str
            les colones dans lesquelles insérer

        values : str
            valeurs à insérer 
        
        table : str
            table sur laquelle exécuter la requête
        
        Retours
        -------
        retourne : bool
            True si insert réussi
    """
    c = sqlite3.connect("bdd.db")

    req = f"INSERT INTO {table} ({keys}) VALUES ({values});"

    try:
        c.execute(req)
        c.commit()
        return True

    except sqlite3.DatabaseError as e:
        print(req)
        print(e)
        return False


def req_sql(req):
    """
        Exécute une requête SQL

        Paramètres 
        -----------
        req : str
            Requête à exécuter
        
        Retours
        -------
        retourne : list de dict
            Résultat de la requête SQL exécutée. Les clefs sont les noms des colones demandées
    """
    c = sqlite3.connect("bdd.db")

    try:
        result = c.execute(req)

        # Transformation en dictionaire
        elems = req.split('SELECT ')[1].split(' FROM')[0].split(', ')  # Extraction des colones
        res = []
        for row in result:
            dic = {}
            for i in range(len(elems)):
                dic[elems[i]] = row[i]
            res.append(dic)

        return res

    except sqlite3.DatabaseError as e:
        print(req)
        print(e)
        return False


def get_ville(ville, elements="id_ville, cp, commune, dept, region, coord"):
    """
        Récupère des informations sur une ville

        Paramètres 
        -----------
        ville : str
            Ville à étudier sous forme de nom complet ou code insee ou code postal
        
        elements : str (facultatif)
            Colones à selectionner
            Par défaut : toutes les colones

        Retours
        -------
        retourne : list de dict
            Informations sur la ville. Les clefs sont les noms des colones demandées.
    """
    c = sqlite3.connect("bdd.db")

    req = f"SELECT {elements} FROM Villes WHERE commune = '{ville}' OR id_ville = '{ville}' OR cp = '{ville}' LIMIT 1;"

    result = c.execute(req)

    # Transformation en dictionaire
    elems = elements.split(', ')
    res = []
    for row in result:
        dic = {}
        for i in range(len(elems)):
            dic[elems[i]] = row[i]
        res.append(dic)

    return res[0]


def select_sql(elements=TOUTES_COLONES, cond="LIMIT 10"):
    """
        Exécute une requête SQL

        Paramètres 
        -----------
        elements : str (facultatif)
            Colones à selectionner
            Par défaut : Toutes les colones
        
        cond : str (facultatif)
            Condition(s) à utiliser dans la requête. 
            Par défaut : Limite à 10 résultats
        
        Retours
        -------
        retourne : list de dict
            Résultat de la requête SQL exécutée. Les clefs sont les noms des colones demandées
    """
    c = sqlite3.connect("bdd.db")

    req = (f"SELECT {elements} FROM Profils "
           "LEFT JOIN Hobbies ON Profils.id_util = Hobbies.id_hobbies "
           "LEFT JOIN Physique ON Profils.id_util = Physique.id_phys "
           "LEFT JOIN Villes ON Profils.ville = Villes.id_ville "
           f"{cond};")

    result = c.execute(req)

    # Transformation en dictionaire
    elems = elements.split(', ')
    res = []
    for row in result:
        dic = {}
        for i in range(len(elems)):
            dic[elems[i]] = row[i]
        res.append(dic)

    return res


if __name__ == '__main__':
    # print(insert_sql("Profils", "id_util, nom, prenom, age, mail, sexe, orientation, ville, hobbies,
    # traits_physiques", "0, 'ozanne', 'colin', 17, 'colin@ozanne.fr', 'h', 'f', 35001, 0, 0"))
    infos = select_sql()
    print(infos)
